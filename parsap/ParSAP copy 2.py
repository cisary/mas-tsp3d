'''
Created on Jun 2, 2009

@author: trevor

To run ParSAP:

First go to the Configuration section of ParSAP and set all of the configuration variables.
If there are remote machines, run: /usr/bin/ppserver.py -a  on each of them.
Then run: python ParSAP.py <num_cities> <height> <width>


'''
import PrintMap
import pp
import pylibmc
import copy, os, operator
import sys,time, math, random

class TravelingSalesman(object):
	'''
	Parallel Simulated Annealing
	'''
	def __init__(self,inputfile):
		'''
		Constructor
		'''        
		self.orderedList = dict() #the main, shared copy of the cities for the whole program
		self.citiescoords = dict()
		self.inputfile=inputfile
		
		
	def run(self,method):
		start_time = time.time()

		coolSched = dict({
		0:.95, 
		1:.9999,
		2:.90,
		3:.9999,
		4:.9988,
		5:.9978,
		6:.9928,
		7:.9338,
		8:.9887,
		9:.9988,
		10:.9978,
		11:.9128,
		12:.898,
		13:.913,
		14:.817,
		15:.7988,
		16:.8978,
		17:.878,
		18:.977,
		19:.888,
		20:.911,
		21:.9128,
		22:.898,
		23:.913,
		24:.817,
		25:.7988,
		26:.8978,
		27:.878,
		28:.977,
		29:.888,
		30:.911,
		})
		'''
		give each CPU a cooling schedule
		coolSched = dict({
		0:.195, 
		1:.19999,
		2:.190,
		3:.19999,
		4:.19988,
		5:.19978,
		6:.19928,
		7:.19338,
		8:.19887,
		9:.19988,
		10:.19978,
		11:.19128,
		12:.1898,
		13:.1913,
		14:.1817,
		15:.17988,
		16:.18978,
		17:.1878,
		18:.1977,
		19:.1888,
		20:.1911,
		21:.9128,
		22:.898,
		23:.913,
		24:.817,
		25:.7988,
		26:.8978,
		27:.878,
		28:.977,
		29:.888,
		30:.911,
		})
		'''
		
		#t0 = (-(int(sys.argv[2])/int(sys.argv[3])) / (int(sys.argv[1])*int(sys.argv[1]))) / math.log(.8)
		t0 = 9
		'''
		Create an initial temperature.  This needs to be worked out - the math here is kind of hand-wavy
		'''
		
		seed = 42
		#seed = 0
		'''
		Set the seed to a value so that you can compare results against the same initial layout of cities.
		if seed == 0, current system time is used as seed
		'''
		
		HillClimb_Only = False
		if (method=="HillClimbing"):
			print "HILLLLLLLL"
			HillClimb_Only = True
		
		'''
		For the sake of testing, you can specify Hill Climb Only, which means that only positive changes
		are accepted.  "False" will be Simulated Annealing as usual.
		'''
		
		print "connecting to memcache at 158.196.32.205:11100"
		mc = pylibmc.Client(['158.196.32.205:11100'])#,binary=True,behaviors={"tcp_nodelay": True})
		print "ok"
		counter = mc.get("PPSERVER.COUNTER")
		print "ppservers available: "+str(counter)
		ppservers=()
		i=0
		print "fetching ppserver adresses and ports"
		while i<int(counter):
			ip = str(mc.get("PPSERVER"+str(i)+"IP"))
			port  = str(mc.get("PPSERVER"+str(i)+"PORT"))
			ppservers=ppservers+(ip+":"+port,)
			print "node" + str(i)+": "+str(ip)+":"+str(port)
			i+=1
		print "ok"
		print ""
		print "connecting to all nodes"
		job_server = pp.Server(ncpus=int(counter),ppservers=ppservers,secret="kluc")
		print "ok - starting pp with", job_server.get_ncpus(), " workers"
		
		remoteCPUS = int(counter)
		localCPUS = 1
		numCPUS=remoteCPUS + localCPUS
		'''
		Set up ParallelPython stuff
		'''
		
		myPSA = TravelingSalesman("input")
		myPSA.createCities(seed)
		'''
		Create myPSA instance and give it arguments
		Then populated it with the initial configuration of random cities
		'''
		    
		best_length = myPSA.getLength(myPSA.orderedList)
		best_cities = copy.deepcopy(myPSA.orderedList)
		#a deepcopy is probably completely unecessary here.
		print "Initial Length of random map: ", best_length
		#PrintMap.printImage(myPSA.orderedList, myPSA.height, myPSA.width, "mydwg.orig",best_length)
		'''
		Hold on to the initial map, and print it out to a PNG file.
		'''
		
		
		
		curCPU = 0
		jobs = dict()
		for i in range(0,numCPUS):
			print "i="+str(i)+"job: "+str()
			jobs[i] = job_server.submit(myPSA.SA, (best_cities,t0,coolSched[i],HillClimb_Only),(), ("copy","math","random","operator"))
		#Given the number of CPUs we're using, submit some jobs.
		
		job_server.wait()
		
		###############
		#useful for figuring out which core you're in, on an SMP
		#mycore = open("/proc/%i/stat" % os.getpid()).read().split()[38]
		#print "core:", mycore, "executing stage"
		################
		
		sameCount = 0
		flag = True
		while flag:
			if(jobs[curCPU].finished):
				#When a job is completed, grab its result, evaluate it to see if the new length is better
				#and either keep it or toss it. Then restart this processor on the current best result
				result = jobs[curCPU]()
				myLength = result[1]
				print "Result length from CPU #:", curCPU, " was: ", myLength
				print "Cities: "+str()
				print str(self.citiescoords)
				for k,v in result[0].items():
					print str(myPSA.citiescoords[str("["+"%.3f" %v[0]+","+"%.3f" %v[1]+","+"%.3f" %v[2]+"]")])
					
					#for x,y,z in result[0][res]:
					#	print res
					#print res[1]
					#print res[2]
				if myLength < best_length:
					best_cities = result[0]
					best_length = myLength
					sameCount = 0
				else:
					sameCount = sameCount + 1
				print "Giving job to CPU #: ", curCPU, " with length of: ", best_length
				jobs[curCPU] = job_server.submit(myPSA.SA, (best_cities,t0,coolSched[curCPU],HillClimb_Only),(), ("copy","math","random","operator"))
			else:
				#This should probably be some sort of delay or something, so that this process doens't hog a bunch of time.
				#Once the total number of processors is high enough (in cluster mode), this will stop mattering.
				pass
			if sameCount > 50:
				#If we have 50 of the same answer, we quit
				flag = False
			curCPU = operator.mod((curCPU + 1),numCPUS)
			job_server.wait()
			#check up on our CPUs, one at a time.	
		print "Final Length of map: ", best_length
		#PrintMap.printImage(best_cities, myPSA.height, myPSA.width, "mydwg.final",best_length)
		
		job_server.print_stats()
		print "DONE"
    

	def createCities(self,sd):
		citycounter=0
		counter=0
		x=0.0
		y=0.0
		z=0.0
		
		print "reading input file: "
		for line in self.inputfile.readlines():
			counter+=1
			if counter == 1:
				x=float(line)
			elif counter == 2:
				y=float(line)
			elif counter == 3:
				z=float(line)
			elif counter == 4:
				#self.OrderedList[citycounter] = ((float(x)) , (float(y)) , (float(z)))
				#self.cities[(x,y,z)]=citycounter
				self.orderedList[citycounter] = ((float(x)),(float(y)),(float(z)))
				#str(x)+"."+str(z)+"."+str(z))]=str()
				self.citiescoords[str("["+"%.3f" %x+","+"%.3f" %y+","+"%.3f" %z+"]")]=citycounter
				print "["+"%.3f" %x+","+"%.3f" %y+","+"%.3f" %z+"]="+str(citycounter)
				#print (str(x))+"."+(str(y))+"."+(str(z))
				citycounter+=1
				counter=0
				
		f.close()
		print "citycounter:"+str(citycounter)
		print "citycoords"+str(self.citiescoords)
		#print "i:"+str(i)
		for i in xrange(0,citycounter):
			print "i:"+str(i)+"= "+str(self.orderedList[i])
		self.num = citycounter - 1
		
        #prng = random.Random()
       # if sd != 0:
          #      prng.seed(sd)
        #for i in range(0,self.numCities):
         #   self.orderedList[i] = ((prng.random()*self.width),(prng.random()*self.height))

	def getLength(self,cities):
		'''
		 takes a dictionary of cities, where it assumes keys 0 -> (n-1) are present
		'''
		runningTotal = 0
		x1 = (cities[len(cities)-1])[0] #Start with the very last city
		y1 = (cities[len(cities)-1])[1]
		z1 = (cities[len(cities)-1])[2]
		for i in range(0,len(cities)):
			x2 = (cities[i])[0]  
			y2 = (cities[i])[1]
			z2 = (cities[i])[2]
			
			d = math.sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) + ((z2-z1)*(z2-z1)) )  #and compute the distance between (x1,y1) and (x2,y2)
			runningTotal = runningTotal + d
			
			x1 = x2
			y1 = y2
			z1 = z2
			
		return runningTotal #return the final length of the cycle

	def SA(self,cities,temperature,cooling,hillclimb_only):
		'''
		The Simulated Annealing algorithm
		'''        
		
		localCities = copy.deepcopy(cities) #create a local copy of the cities for modification, without affecting the main copy
		
		bestLength = self.getLength(localCities)
		
		for numSame in range(0,2000):
			#numSame is used to see if we dont make any changes after 1000 tries. This is 100% arbitrary,
			#but higher numbers tend to mean better results, up to a point.
			
			index = random.randint(0,self.num-1)
			tempCity = localCities[index]
			next = random.randint(0,self.num-1)
			while(next == index): 
			    next = random.randint(0,self.num-1)
			localCities[index] = localCities[next]
			localCities[next] = tempCity
			#Here, we select two cities at random and swap them.  Another option is to select the next city
			#in the dictionary, and to use that.  This has its pros and cons.
			
			tempLength = self.getLength(localCities)
			
			if tempLength < bestLength:
				#Always accept good changes
				bestLength = tempLength
				numSame = 0
			    
			else:
				if not hillclimb_only:
					r = random.random()
					e = math.exp( -((tempLength - bestLength) / temperature ))
					#this is the typical way to sometimes select worse results in SA.
					if r < e:
						bestLength = tempLength
						numSame = 0
					else: #revert to the original configuration. This one wasn't accepted.
						tempCity = localCities[index]
						localCities[index] = localCities[next]
						localCities[next] = tempCity
						
			temperature = cooling * temperature
            #Lower the temperature (cooling is always < 1)
		return (localCities,bestLength)
        #Returns a tuple containing the list of cities AND the length.  This is because the work to calculate the length
        #should not be done in the main thread.