'''
4.12.2012
@author: webmaster@cisary.com

Implementation of Traveling Salesman Problem solver by simulated annealing and hill climbing algorithms

Methods:
	HillClimbing
	SimulatedAnnealing

Based on Parallel Simulated Annealing in Python by 
https://code.google.com/p/parsap/

'''

import pylibmc
import copy, os, operator
import sys,time, math, random

class TravelingSalesman(object):
	'''
	Simulated Annealing solver for TSP 3D problem
	'''
	def __init__(self,inputfile,f_counter,f_fitness,f_popsize,f_solution,config):

		self.orderedList = dict()
		self.citiescoords = dict()
		self.inputfile = inputfile
		self.f_counter = f_counter
		self.f_fitness = f_fitness
		self.f_popsize = f_popsize
		self.f_solution = f_solution
		self.config=config
		
	def run(self,method):
		start_time = time.time()

		t0 = self.config["t0"]
		cool = .9876

		seed = 42
		
		HillClimb_Only = False
		tempextension = "sa"
		if (method=="HillClimbing"):
			tempextension = "hc"
			HillClimb_Only = True

		self.readInput(seed)

		best_length = self.getLength(self.orderedList)
		best_cities = copy.deepcopy(self.orderedList)
		print "Best initial: ", best_length
		initial_length=best_length
		self.f_counter.seek(0, 0)
		self.f_counter.write("1\n")
		self.f_counter.flush()
		self.f_fitness.seek(0, 0)
		self.f_fitness.write(str(best_length)+"\n")
		self.f_fitness.flush()
		
		sameCount = 0
		flag = True
		counter = 1
		solutions = 0
		average = 0
		while (sameCount < self.config["samecountmax"]):
		
			result = self.SA(best_cities,t0,cool,HillClimb_Only)
			myLength = result[1]
			
			temp_fitness = open("./tmp/"+tempextension+"_fitness_"+str(counter),'w+')
			temp_fitness.truncate()
			temp_fitness.write(str(myLength)+"\n")
			temp_fitness.flush()
			temp_fitness.close()
			
			print str(counter)+": "+str(myLength)

			self.f_counter.truncate()
			self.f_counter.seek(0, 0)
			self.f_counter.write(str(counter)+"\n")
			self.f_counter.flush()
			
			average+=myLength
			
			counter+=1

			if myLength < best_length:
				best_cities = result[0]
				best_length = myLength
				solutions += 1
				sameCount = 0
				
				temp_solution = open("./tmp/"+tempextension+"_solution_"+str(counter),'w+')
				temp_solution.truncate()
				temp_solution.write("[\n")
				path=""
				first=""
				b=True
				for k,v in result[0].items():
					cityorder=str(self.citiescoords[str("["+"%.3f" %v[0]+","+"%.3f" %v[1]+","+"%.3f" %v[2]+"]")])
					temp_solution.write(cityorder+"\n")
					if (b):
						first=cityorder
						b=False
					path+=cityorder+" -> "
				print "!! NEW SOLUTION: ("+path+" "+first+")"
				temp_solution.write("]\n")
				temp_solution.flush()
				temp_solution.close()
				
			else:
				sameCount += 1

		print "Same count "+str(self.config["samecountmax"])+" reached in "+ str(counter)+ " iteration"
		average=float(average) / float(counter) 
		
		print ""
		print "Solutions: "+str(solutions)
		print ""
		print "Initial: " + str(initial_length)
		print "Average: " + str(average)
		print ""
		print "Best   : ", best_length

	def readInput(self,sd):
		citycounter=0
		counter=0
		x=0.0
		y=0.0
		z=0.0
		print "Reading input file: "
		for line in self.inputfile.readlines():
			counter+=1
			if counter == 1:
				x=float(line)
			elif counter == 2:
				y=float(line)
			elif counter == 3:
				z=float(line)
			elif counter == 4:
				self.orderedList[citycounter] = ((float(x)),(float(y)),(float(z)))
				self.citiescoords[str("["+"%.3f" %x+","+"%.3f" %y+","+"%.3f" %z+"]")]=citycounter
				citycounter+=1
				counter=0
		print "citycounter:"+str(citycounter)
		for i in xrange(0,citycounter):
			print "i:"+str(i)+" = "+str(self.orderedList[i])
		self.num = citycounter - 1

	def getLength(self,cities):
		'''
		 takes a dictionary of cities, where it assumes keys 0 -> (n-1) are present
		'''
		runningTotal = 0
		x1 = (cities[len(cities)-1])[0] 
		y1 = (cities[len(cities)-1])[1]
		z1 = (cities[len(cities)-1])[2]
		for i in range(0,len(cities)):
			x2 = (cities[i])[0]  
			y2 = (cities[i])[1]
			z2 = (cities[i])[2]
			runningTotal += math.sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) + ((z2-z1)*(z2-z1)) )
			x1 = x2
			y1 = y2
			z1 = z2
		return runningTotal #return the final length of the path

	def SA(self,cities,temperature,cooling,hillclimb_only):
		'''
		The Simulated Annealing algorithm
		'''        
		localCities = copy.deepcopy(cities) #create a local copy of the cities for modification, without affecting the main copy
		
		bestLength = self.getLength(localCities)
		
		for numSame in range(0,2000):
			#numSame is used to see if we dont make any changes after 1000 tries. This is 100% arbitrary,
			#but higher numbers tend to mean better results, up to a point.
			
			index = random.randint(0,self.num-1)
			tempCity = localCities[index]
			next = random.randint(0,self.num-1)
			while(next == index): 
			    next = random.randint(0,self.num-1)
			localCities[index] = localCities[next]
			localCities[next] = tempCity

			tempLength = self.getLength(localCities)
			
			if tempLength < bestLength:
				#Always accept good changes
				bestLength = tempLength
				numSame = 0
			else:
				if not hillclimb_only:
					r = random.random()
					e = math.exp( -((tempLength - bestLength) / temperature ))
					#this is the typical way to sometimes select worse results in SA.
					if r < e:
						bestLength = tempLength
						numSame = 0
					else: #revert to the original configuration. This one wasn't accepted.
						tempCity = localCities[index]
						localCities[index] = localCities[next]
						localCities[next] = tempCity
						
			temperature = cooling * temperature
            #Lower the temperature (cooling is always < 1)
		return (localCities,bestLength)
        #Returns a tuple containing the list of cities AND the length.  This is because the work to calculate the length
        #should not be done in the main thread.