'''
Created on Jun 14, 2009

@author: trevor
'''

import random,copy,math

class TravSales(object):

	def get_city(self, key):
		my_dict=self.cities
		return reduce(dict.get, key.split("."), my_dict)
	
	def set_city(self, key, value):
		my_dict=self.cities
		key = key.split(".")
		my_dict = reduce(dict.get, key[:-1], my_dict)
		my_dict[key[-1]] = value
	
	def del_city(self, key):
		my_dict=self.cities
		key = key.split(".")
		my_dict = reduce(dict.get, key[:-1], my_dict)
		del my_dict[key[-1]]
    
	def __init__(self,inputfilename):
		self.OrderedList = dict()
		self.cities = dict()
		citycounter=0
		counter=0
		x=0.0
		y=0.0
		z=0.0
		f = open(inputfilename,'r')
		print "reading input file: "+str(inputfilename)
		for line in f.readlines():
			counter+=1
			if counter == 1:
				x=float(line)
			elif counter == 2:
				y=float(line)
			elif counter == 3:
				z=float(line)
			elif counter == 4:
				self.OrderedList[citycounter] = ((float(x)) , (float(y)) , (float(z)))
				self.cities["%.3f" %x+"."+"%.3f" %y+"."+"%.3f" %z]=citycounter
				
				#str(x)+"."+str(z)+"."+str(z))]=str()
				citycounter+=1
				counter=0
				
		f.close()
		print "citycounter:"+str(citycounter)
		#print "i:"+str(i)
		for i in xrange(0,citycounter):
			print "i:"+str(i)+"= "+str(self.OrderedList[i])
		self.num = citycounter - 1
            

def Creator(inputfilename):
    #return an initial instance of the class
    return TravSales(inputfilename)

def Mutator(obj):
    #return a neighbor of this class
    temp = copy.deepcopy(obj)
    
    index = random.randint(0,obj.num-1)
    index2 = random.randint(0,obj.num-1)
    while(index == index2):
        index2 = random.randint(0,obj.num-1)
    tempCity = temp.OrderedList[index]
    temp.OrderedList[index] = obj.OrderedList[index2]
    temp.OrderedList[index2] = temp.OrderedList[index]
    return temp

def Path(obj):
    x = (obj.OrderedList[obj.num-1])[0] 
    y = (obj.OrderedList[obj.num-1])[1]
    z = (obj.OrderedList[obj.num-1])[2]
    
    #last = obj.cities[str(x)+"."+str(y)+"."+str(z)]
    print "["
    print obj.cities[(x,y,z)]
    for i in range(0,obj.num):
        x = (obj.OrderedList[i])[0]  
        y = (obj.OrderedList[i])[1]
        z = (obj.OrderedList[i])[2]
        print obj.cities[(x,y,z)]
        #print obj.cities[str(x)+"."+str(y)+"."+str(z)]
    print "]"
 
    
def Printable(obj):
    #be able to print out the class
    runningTotal = 0
    x1 = (obj.OrderedList[obj.num-1])[0] #Start with the very last city
    y1 = (obj.OrderedList[obj.num-1])[1]
    z1 = (obj.OrderedList[obj.num-1])[2]
    for i in range(0,obj.num):
        x2 = (obj.OrderedList[i])[0]  
        y2 = (obj.OrderedList[i])[1]
        z2 = (obj.OrderedList[i])[2]
            
        d = math.sqrt( ((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)) + ((z2-z1)*(z2-z1)) )  #compute the distance
        runningTotal = runningTotal + d
            
        x1 = x2
        y1 = y2
        z1 = z2
        
    return runningTotal

def Comparor(obj_one, obj_two):
    #determine if obj_one is BETTER than obj_two
    if Printable(obj_one) < Printable(obj_two):
        return True
    else:
        return False
    
def Differencer(obj_one, obj_two):
    #if obj_one is better, return a -1, if obj_two is better, return a 1
    #this assumes we're trying to minimize a value.  Reverse it for maximizing 
    if Printable(obj_one) < Printable(obj_two):
        return -1
    else:
        return 1