'''
Created on Jun 14, 2009

@author: trevor
'''

import sys,math,random,operator,os
import pylibmc
import pp

def SA(MyObj,module_name,t0,cool):

    module = __import__(module_name)
    for numSame in range(0,100):
    
        neighbor = module.Mutator(MyObj)
        if module.Comparor(neighbor,MyObj):
            MyObj = neighbor
            numSame = 0
        else:
            r = random.random()
            e = math.exp( -(module.Differencer(neighbor,MyObj) / t0) )
            if r < e:
                MyObj = neighbor
                numSame = 0
        t0 = t0 * cool
        
    return MyObj

class SAModule:

	def __init__(self,modulename):
		self.name=modulename

	def run(self,inputfilename):
		print "connecting to memcache at 158.196.32.205:11100"
		mc = pylibmc.Client(['158.196.32.205:11100'])#,binary=True,behaviors={"tcp_nodelay": True})
		print "ok"
		counter = mc.get("PPSERVER.COUNTER")
		print "ppservers available: "+str(counter)
		ppservers=()
		i=0
		print "fetching ppserver adresses and ports"
		while i<int(counter):
			ip = str(mc.get("PPSERVER"+str(i)+"IP"))
			port  = str(mc.get("PPSERVER"+str(i)+"PORT"))
			ppservers=ppservers+(ip+":"+port,)
			print "node" + str(counter)+": "+str(ip)+":"+str(port)
			i+=1
		print "ok"
		print ""
		print "connecting to all nodes"
		job_server = pp.Server(ncpus=int(counter),ppservers=ppservers,secret="kluc")
		print "ok - starting pp with", job_server.get_ncpus(), " workers"
		
		remoteCPUS = int(counter)
		localCPUS = 1
		numCPUS=remoteCPUS + localCPUS

		
		#import user's module
		module_name = self.name
		module = __import__(module_name)
		
		#create an initial version of the user's object
		CurObj = module.Creator(inputfilename)
		print "INITIAL CONFIG: ", module.Printable(CurObj)
		
		####CONFIG
		temperature = 10
		cool = .95
		numSameResultMax = 50
		####END_CONFIG
		
		
		#submit a few jobs to the CPUs
		curCPU = 0
		jobs = dict()
		for i in range(0,numCPUS):
			jobs[i] = job_server.submit(SA, (CurObj,module_name,temperature,cool),(), ("math","random"))
		
		sameCount = 0
		flag = True
		while flag:
			#if(jobs[curCPU].finished):
			#When a job is completed, grab its result, evaluate it to see if the new value is better
			#and either keep it or toss it. Then restart this processor on the current best result
			result = jobs[curCPU]()
			print "Result value from CPU #:", curCPU, " was: ", module.Printable(result)
			print "Path:"
			module.Path(result)
			if module.Comparor(result,CurObj):
				CurObj = result
				sameCount = 0
				
				#	print "result: "+str(result)
			elif (result != float(0)):
				sameCount = sameCount + 1
			print "Giving job to CPU #: ", curCPU, " with value of: ", module.Printable(CurObj)
			jobs[curCPU] = job_server.submit(SA, (CurObj,module_name,temperature,cool),(), ("math","random"))
			job_server.wait()
			if sameCount > numSameResultMax:
				#If we have numSameResultMax of the same answer, we quit
				flag = False
			curCPU = operator.mod((curCPU + 1),numCPUS)
	        
		job_server.print_stats()
	            
	    
    