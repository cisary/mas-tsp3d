'''
Created on Jun 2, 2009

@author: trevor
'''
import Image, ImageDraw, ImageFont
import sys
        
def printImage(cityList,height,width,fname,length):
    '''
    Prints out a list of cities onto a PNG with lines between them.
    The order of the cities determines the ordering of the lines
    '''
    myImage = Image.new("RGBA",(int(height),int(width)),"white")
    #create a new Image
    draw = ImageDraw.Draw(myImage)
    #create a new ImageDraw instance
    
    prevx = (cityList[len(cityList)-1])[0]
    prevy = (cityList[len(cityList)-1])[1]
    
    for i in range(0,len(cityList)):
        #for each city, draw a rectangle at that X,Y location, in black
        #then draw a line between the previous city and the current city        
        draw.rectangle([( (cityList[i])[0] -3, (cityList[i])[1]-3),( (cityList[i])[0] +3, (cityList[i])[1]+3)], "black")
        draw.line((prevx,prevy, (cityList[i])[0] , (cityList[i])[1]),fill="black")
        prevx = (cityList[i])[0]#.getX()
        prevy = (cityList[i])[1]#.getY()
    
    
    fontPath = "/usr/share/fonts/truetype/ttf-dejavu/DejaVuSans.ttf"
    #oh boy. Picking a proper font path? How do I choose "system default", because there are no guarantees that this will
    #be running on linux only.
    myFont = ImageFont.truetype(fontPath,16)
    #sure... 16 sounds ok.
    strText = "Length = " + str(length)
    draw.text((10,10), strText, "black", font=myFont)
    #print out our text at x=10,y=10
    fname = fname + ".png"
    #append a png file extension onto it.
    myImage.save(fname,"PNG")
    #save it as a PNG file
        
        
        
        