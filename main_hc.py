#-*- coding: utf8 -*- 
# main_hc.py

'''

'''

import os, sys
os.sys.path.append(r'./parsap')
from ParSAP import TravelingSalesman

if __name__ == '__main__':

	f_input = open(str(sys.argv[1]),'r')
	f_counter = open(str(sys.argv[2]),'w+')
	f_fitness = open(str(sys.argv[3]),'w+')
	f_popsize = open(str(sys.argv[5]),'w+')
	f_solution = open(sys.argv[4],'w+')
	
	configuration = dict({
	"coolingfactormin":0.7,
	"coolingfactormax":0.99999,
	"t0":9,
	"samecountmax":10
	})

	TravelingSalesman(f_input,f_counter,f_fitness,f_popsize,f_solution,configuration).run("HillClimbing")
	
	f_input.close()
	f_counter.close()
	f_fitness.close()
	f_popsize.close()
	f_solution.close()


