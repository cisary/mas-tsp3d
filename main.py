#-*- coding: utf8 -*- 
# main.py
"""

Implementacia problemu obchodneho cestujuceho (TSP) v 3D

"""

import sys
from math import sqrt
from pygene.gene import FloatGene, FloatGeneMax, FloatGeneRandom
from pygene.organism import Organism, MendelOrganism
from pygene.population import Population

"""
Pocet generacii genetickeho algoritmu
"""

numGenerations=int(sys.argv[6])

"""
Konfiguracia
"""
geneRandMin = float(sys.argv[8])
geneRandMax = float(sys.argv[9])
print sys.argv[10]
geneMutProb = float(sys.argv[10])

"""
Ak sa nepouziva FloatGeneRandom
"""
geneMutAmt = .5        

popInitSize = int(sys.argv[11])
popChildCull = int(sys.argv[14])
popChildCount = int(sys.argv[13])

"""
Pocet rodicov, ktory sa budu pridavat do novej generacie
"""
popIncest = int(sys.argv[15])      

"""
Pravdepodobonost mutacie
"""
popNumMutants = float(sys.argv[12])  

"""
Pocet nahodnych organizmov pridanych do kazdej generacie
"""
popNumRandomOrganisms = int(sys.argv[7])
mutateOneOnly = False
BaseGeneClass = FloatGene
BaseGeneClass = FloatGeneMax
#BaseGeneClass = FloatGeneRandom
OrganismClass = MendelOrganism
mutateAfterMating = True
"""
Pravdepodobnost krizenia
"""
crossoverRate = float(sys.argv[16])

"""
Kazdy gen reprezentuje prioritu cesty do daneho mesta
"""
class CityPriorityGene(BaseGeneClass):

    randMin = geneRandMin
    randMax = geneRandMax
    mutProb = geneMutProb
    mutAmt = geneMutAmt


"""
Trieda reprezentujuca jedno mesto jeho poradovym cislom a suradnicami.
Navyse pocita vzialenost z ineho mesta.
"""
class City:
    
    """
    Konstruktor vytvori objekt mesta
    """
    def __init__(self, order, x=None, y=None, z=None):
        self.order = order
        self.x = x
        self.y = y
        self.z = z 
        
    """
    Pocita vzdialenost z tohto mesta do ineho 
    """
    def __sub__(self, other):
        dx = self.x - other.x
        dy = self.y - other.y
        dz = self.z - other.z
        return sqrt(dx * dx + dy * dy + dz * dz)

    def __repr__(self):
        return "<City %s at (%.2f, %.2f, %.2f)>" % (self.order, self.x, self.y, self.z)

cities = [] 
citycounter=0
counter=0
x=1.1
y=2.2
z=3.3

f = open(sys.argv[1],'r')

for line in f.readlines():
	counter+=1
	if counter == 1:
		x=float(line)
	elif counter == 2:
		y=float(line)
	elif counter == 3:
		z=float(line)
	elif counter == 4:
		cities.append(City("%s"%citycounter,float(x),float(y),float(z)))
		citycounter+=1
		counter=0
		
f.close()


cityOrders = [city.order for city in cities]

cityCount = len(cities)

cityDict = {}
for city in cities:
    cityDict[city.order] = city

priInterval = (geneRandMax - geneRandMin) / cityCount
priNormal = []
for i in xrange(cityCount):
    priNormal.append(((i+0.25)*priInterval, (i+0.75)*priInterval))

genome = {}
for order in cityOrders:
    genome[order] = CityPriorityGene

"""
Jeden pseudo organizmus reprezentuje riesenie problemu
obchodneho cestujuceho
"""
class TSPSolution(OrganismClass):
    genome = genome
    mutateOneOnly = mutateOneOnly
    crossoverRate = crossoverRate
    numMutants = 0.3

    """
    Vracia vzdialenost celej cesty - fitness hodnotu problemu
    """
    def fitness(self):
        distance = 0.0
        """
        Ziskanie objektov miest podla priority
        """
        sortedCities = self.getCitiesInOrder()

        """
        Zaciname prvym mestom a pocitame vzdialenosti k poslednemu
        """
        for i in xrange(cityCount - 1):
            distance += sortedCities[i] - sortedCities[i+1]
        
        """
        Pripocitame cestu k prvemu mestu (navrat)
        """
        distance += sortedCities[0] - sortedCities[-1]
        return distance


    """
    Vracia zoznam miest zoradenych podla hodnot
    priorit ziskanych z genov jednotlivych organizmov
    """
    def getCitiesInOrder(self):
        sorter = [(self[order], cityDict[order]) for order in cityOrders]
        """
        Zoradenie
        """
        sorter.sort()
        """
        Ziskanie jednotlivych miest
        """
        sortedCities = [tup[1] for tup in sorter]
        return sortedCities


    """
    Upravuje geny
    """
    def normalise(self):
        genes = self.genes
        for i in xrange(2):
            sorter = [(genes[order][i], order) for order in cityOrders]
            sorter.sort()
            sortedGenes = [tup[1] for tup in sorter]
            

"""
Objekt populacie organizmov
"""
class TSPSolutionPopulation(Population):
    initPopulation = popInitSize
    species = TSPSolution
    childCull = popChildCull
    """
    Pocet potomkov, ktory sa maju vytvorit po kazdej populacii
    """
    childCount = popChildCount
    """
    "incest" - Pocet najlepsich rodicov, ktory
    sa pridaju do nasledujucej generacie
    """
    incest = popIncest
    mutants = popNumMutants
    numNewOrganisms = popNumRandomOrganisms
    mutateAfterMating = mutateAfterMating


"""
Hlavna metoda programu
"""
def main():
    
    """
    Vytvorenie povodnej populacie
    """
    pop = TSPSolutionPopulation()
    
    """
    Samotna evolucia
    """
    i = 0
    while i<=numGenerations:
		pop.gen()
		
		i += 1
		f = open(str(sys.argv[2]),'w')
		f.truncate()
		f.write(str(i)+"\n")
		f.flush()
		f.close()
		
		
		f = open(str(sys.argv[3]),'w')
		f.truncate()
		f.write(str(pop.best().fitness())+"\n")
		f.flush()
		f.close()
		
		solution = pop.best()
		sortedCities = solution.getCitiesInOrder()
		
		print pop.best().fitness()
		f = open(sys.argv[4],'w')
		f.write("[\n")
		for city in sortedCities:
			f.write(str(city.order)+"\n")
		f.write(str(sortedCities[0].order)+"\n")
		f.write("]\n")
		f.flush()
		f.close()
		
		f = open(str(sys.argv[5]),'w')
		f.truncate()
		f.write(str(len(pop))+"\n")
		f.flush()
		f.close()

       
        
        #print 
        #print "%s" % (int(pop.best().fitness()))
        

    """
    Ziskanie najlepsieho organizmu
    """
    solution = pop.best()
    """

    sortedCities = solution.getCitiesInOrder()
    
    
    f.write("")
    f.flush()
    f.close()"""
    
if __name__ == '__main__':
    main()





